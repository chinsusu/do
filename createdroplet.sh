#!/bin/sh

# Deploy our cool tool to DigitalOCean.
# Make sure you set the TOKEN environment variable to your Digitialocean API token before running.

set -e     # Stop on first error
set -u     # Stop if an unbound variable is referenced

userdata=`cat ./user-data.yaml`

curl -X POST https://api.digitalocean.com/v2/droplets \
-H "Content-Type: application/json" \
-H "Authorization: Bearer 0e3633a628cc153ab40046bfd9f20cb736486fa273d32f3c7a93d5b3a786fa3b" \
-d '{"name":"test",
     "region":"nyc3",
     "size":"s-4vcpu-8gb",
     "image":"ubuntu-16-04-x64",
     "ssh_keys":null,
     "backups":false,
     "ipv6":true,
     "private_networking":false,
     "volumes": null,
     "user_data":"
'"$userdata"'
"}'