#!/bin/bash
sudo su 
sudo apt-get update -y &&
sudo apt --assume-yes install libmicrohttpd-dev libssl-dev cmake build-essential libhwloc-dev git libuv1-dev &&
cd /usr/local/src/ &&
sudo git clone https://github.com/fireice-uk/xmr-stak &&
sudo mkdir xmr-stak/build &&
cd xmr-stak/build &&
cmake .. -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF &&
make install &&
cd bin/ &&
sudo sysctl -w vm.nr_hugepages=128 &&
sudo bash -c 'cat <<EOT >>/usr/local/src/xmr-stak/build/bin/config.txt
"call_timeout" : 10,
"retry_time" : 30,
"giveup_limit" : 0,
"verbose_level" : 3,
"print_motd" : true,
"h_print_time" : 60,
"aes_override" : null,
"use_slow_memory" : "warn",
"tls_secure_algo" : true,
"daemon_mode" : false,
"flush_stdout" : false,
"output_file" : "",
"httpd_port" : 99,
"http_login" : "",
"http_pass" : "",
"prefer_ipv4" : true,
EOT
' &&
sudo bash -c 'cat <<EOT >>/usr/local/src/xmr-stak/build/bin/cpu.txt
"cpu_threads_conf" :
[
    { "low_power_mode" : 5, "no_prefetch" : true, "affine_to_cpu" : 0 },
    { "low_power_mode" : 5, "no_prefetch" : true, "affine_to_cpu" : 1 },
    { "low_power_mode" : 5, "no_prefetch" : true, "affine_to_cpu" : 2 },
],
EOT
' &&
sudo bash -c 'cat <<EOT >>/usr/local/src/xmr-stak/build/bin/pools.txt
"pool_list" :
[
{
"pool_address" : "worktips.asiapool.space:3333",
"wallet_address" : "WtipVBprbjhF9KmmSxqG9dCRmpWebV6DuRNCtnVTV3ZnAnqUb1nZA6zQWMd4FWrvC9WYojJYwJ1UA8AFqgMi9xm97QjJcKALWX+27f6d3c9b7f68e2f805150f45f661004e6c62022e63c94a91904b273031bbbba.10000",
"rig_id" : "DO6", "pool_password" : "x", "use_nicehash" : true, "use_tls" : false, "tls_fingerprint" : "", "pool_weight" : 1
},
],
"currency" : "cryptonight_lite_v7",
EOT
' &&

sudo bash -c 'cat <<EOT >>/lib/systemd/system/xmrig.service
[Unit]
Description=xmr
After=network.target
[Service]
ExecStart=/usr/local/src/xmr-stak/build/bin/xmr-stak -c /usr/local/src/xmr-stak/build/bin/config.txt --cpu /usr/local/src/xmr-stak/build/bin/cpu.txt -C /usr/local/src/xmr-stak/build/bin/pools.txt
WatchdogSec=1800
Restart=always
RestartSec=60
User=root
[Install]
WantedBy=multi-user.target
EOT
' &&

#!/bin/bash
sudo systemctl daemon-reload &&
sudo systemctl enable xmrig.service &&
sudo systemctl start xmrig.service